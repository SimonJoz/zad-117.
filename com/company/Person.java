package com.company;

public class Person {
    private String name;
    private String surname;
    private int number;

    void setName(String name) {
        this.name = name;
    }

    void setSurname(String surname) {
        this.surname = surname;
    }

    void setNumber(int number) {
        this.number = number;
    }

    @Override
    public String toString() {
        return "Person: " + name + " " + surname + "\n" + "Phone No: " + number;
    }
}

