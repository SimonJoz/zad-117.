package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class MyMethods {
    private Map<Integer, Person> map = new HashMap<>();
    private Map<Integer, Person> map2 = new HashMap<>();

    // create var for Person Obj.
    Person StringParser(Person person, String text) {
        String pattern = "([a-z,A-Z]+)\\s+([a-z,A-Z]+)\\s+([0-9]{9,12})";
        Pattern r = Pattern.compile(pattern);
        Matcher m = r.matcher(text);
        if (m.find()) {
            person.setName(m.group(1));
            person.setSurname(m.group(2));
            person.setNumber(Integer.parseInt(m.group(3)));
        }
        return person;
    }

    // iterate through map
    void display(Map<Integer, Person> map) {
        for (Integer key : map.keySet()) {
            System.out.println(key + ": " + map.get(key));
            System.out.println("=============");
        }
    }
    // create obj from file data
    void readAndCreate() throws FileNotFoundException {
        File file = new File("doc.txt");
        Scanner sc = new Scanner(file);
        int i = 1;
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
            map2.put(i, StringParser(new Person(), line));
            i++;
        }
    }

    Map<Integer, Person> getMap2() {
        return map2;
    }

    Map<Integer, Person> getMap() {
        return map;
    }
}
